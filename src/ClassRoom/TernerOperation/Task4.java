package ClassRoom.TernerOperation;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int b = in.nextInt();
        System.out.println(b*b > 100? 1:2);
    }
}
