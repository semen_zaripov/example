package ClassRoom.TernerOperation;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
//        if (x%2 == 0 ) {
//            System.out.println("Чётное");
//        } else {
//            System.out.println("Нечётное");
//        }
        System.out.println(x%2 == 0? "Чётное" : "Нечётное");
    }

    }

