package ClassRoom.TernerOperation;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        System.out.println(a + b > 100? b:a);
        System.out.println(a + b);
    }
}