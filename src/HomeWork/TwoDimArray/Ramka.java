package HomeWork.TwoDimArray;

import java.util.Random;

public class Ramka {
    public static void main(String[] args) {
        int[][] twoDimArray = new int[4][4];
        Random random = new Random();

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray.length; j++) {
                twoDimArray[i][j] = random.nextInt(10);
            }
        }
        for (int i = 0; i < twoDimArray.length; i++) {

            for (int j = 0; j < twoDimArray.length; j++) {
                if (i == 0) {
                    System.out.print(twoDimArray[i][j] + " ");
                }
                else if (i == twoDimArray.length - 1) {
                    System.out.print(twoDimArray[i][j] +" ");
                }
                else if ( j == 0 || j == twoDimArray.length - 1) {
                    System.out.print(twoDimArray[i][j] +" ");
                }
                else {
                    System.out.print( "  ");
                }
            }
            System.out.println();
        }
    }
}
