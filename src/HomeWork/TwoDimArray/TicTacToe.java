package HomeWork.TwoDimArray;

import java.util.Scanner;

public class TicTacToe {
    public static final String empty = "   ";
    public static final String SIGN_X = " X ";
    public static final String SIGN_O = " O ";
    public static String ActivePlayer;

    public static final int  lines=3, columns=3;
    public static String[][] pole = new String[lines][columns];
    public static int GameStat;
    public static final int STATUS_GAME_CONTINUES=0, STATUS_DRAW=1, STATUS_VICTORY_X =3, STATUS_VICTORY_O=4;
    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args){
        StartGame();
        do{
            inputPlayer();
            BoardAnalysis();
            PrintGameBoard();
            if (GameStat==STATUS_VICTORY_X){
                System.out.println("'X' победил!");
            } else if (GameStat==STATUS_VICTORY_O){
                System.out.println("'O' победил!");
            } else if (GameStat==STATUS_DRAW){
                System.out.println("Игра закончилась ничьей!");
            }
            ActivePlayer = (ActivePlayer==SIGN_X?SIGN_X:SIGN_O);
        }
        while (GameStat== STATUS_GAME_CONTINUES);
    }
    public static void StartGame(){
        for ( int row = 0; row < lines; row++){
            for (int column = 0; column<columns; column++){
                pole[row][column]=empty;
            }
        }
        ActivePlayer = SIGN_X;

    }
    public static void inputPlayer(){

        boolean checkDistance =false;
        do {
            System.out.println("Игрок "+ActivePlayer+"введите строку и столбик через пробел");
            int row = in.nextInt()-1; int column = in.nextInt()-1;
            if(row>=0 && row < lines && column >=0 && column<columns && pole[row][column]==empty){
                pole[row][column] = ActivePlayer;
                checkDistance=true;
            } else {
                System.out.println("Выбранное размещение (" + (row + 1) + "," + (column + 1)
                        + ") нельзя использовать. Поробуй еще раз...");
            }
            if (ActivePlayer.equals(SIGN_X)){
                ActivePlayer=SIGN_O;
            } else {
                ActivePlayer=SIGN_X;
            }
        }
        while (!checkDistance);
    }
    public static void BoardAnalysis(){
        String winner = knowWinner();
        if (winner.equals(SIGN_X)){
            GameStat = STATUS_VICTORY_X;
        } else if (winner.equals(SIGN_O)){
            GameStat = STATUS_VICTORY_O;
        } else if (allCellsAreField()){
            GameStat = STATUS_DRAW;
        } else {
            GameStat = STATUS_GAME_CONTINUES;
        }

    }
    public static boolean allCellsAreField(){
        for (int row = 0; row < lines; row++){
            for (int column = 0; column < columns; column++){
                if (pole[row][column]==empty) {
                    return false;
                }
            }
        }
        return true;
    }
    public static String knowWinner(){

        int Cells = 0;
        for (int row = 0; row<lines;row++){
            for (int column = 0; column<columns; column++){
                if (pole[row][0]!=empty && pole[row][0]==pole[row][column]){
                    Cells++;
                }
            }
            if ( Cells==3){
                return  pole[row][0];
            }
        }

        for (int column = 0; column<columns;column++){
            Cells = 0;
            for (int row = 0; row<lines; row++){
                if (pole[0][column]!=empty && pole[0][column]==pole[row][column]){
                    Cells++;
                }
            }
            if ( Cells==3){
                return  pole[0][column];
            }
        }
        if (pole[0][0]!=empty && pole[0][0]== pole[1][1] && pole[0][0]== pole[2][2]){
            return pole[0][0];
        }
        if (pole[0][2]!=empty && pole[1][1]== pole[0][2] && pole[2][2]== pole[0][2]){
            return pole[0][2];
        }
        return empty;
    }
    public static void PrintGameBoard(){
        for ( int row = 0; row < lines; row++){
            for (int column = 0; column < columns; column++){
                System.out.print(pole[row][column]);
                if ( column!=columns-1){
                    System.out.print("|");
                }
            }
            System.out.println();
            if (row!=lines-1){
                System.out.println("-----------");
            }
        }
        System.out.println();
    }

}
