package HomeWork.TwoDimArray;

import java.util.Random;
import java.util.Scanner;

public class MatrixAddition {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int c = in.nextInt();
        Random random = new Random();
        int[][] array = new int[n][c];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(10);
            }
        }
        int[][] array2 = new int[n][c];
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                array2[i][j] = random.nextInt(10);
            }
        }
        int[][] sum = new int[n][c];
        for ( int i = 0; i < sum.length; i++) {
            for (int j = 0; j < sum[i].length; j++){
                sum[i][j] = array[i][j] + array2[i][j];
            }
        }
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                System.out.print(array[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                System.out.print(array2[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                System.out.print(sum[i][j]+" ");
            }
            System.out.println();
        }
    }
}