package HomeWork.TwoDimArray;

import java.util.Random;
import java.util.Scanner;

public class OutPut {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        Random random = new Random();

        int n = in.nextInt();
        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = random.nextInt(10);
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[j][i] + " ");
            }
            System.out.println();
        }
    }
}
