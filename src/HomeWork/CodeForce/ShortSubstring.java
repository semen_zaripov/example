package HomeWork.CodeForce;

import java.util.Scanner;

public class ShortSubstring {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int i = 0;i<t;i++){
            String s =in.next();
            String shortS = shortSubstring(s);
            System.out.println(shortS);

        }

    }
    public static String shortSubstring(String s){
        String s1 ="";
        s1=s1+s.charAt(0);
        for (int i=1;i<s.length()-1;i+=2){
            s1=s1+s.charAt(i);
        }
        s1=s1+s.charAt(s.length()-1);
        return s1;
    }
}
