package HomeWork.CodeForce;

import java.util.Scanner;

public class BobAndLimak {
   public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int count = 0;
        while (b >= a){
            a = a*3;
            b = b*2;
            count++;
        }
        System.out.println(count);
    }
}
