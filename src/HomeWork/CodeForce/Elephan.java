package HomeWork.CodeForce;

import java.util.Currency;
import java.util.Scanner;

public class Elephan {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int count = 0;
        int CurrentX = 0;

        while (CurrentX != x) {
            if (x - CurrentX >= 5) {
                count++;
                CurrentX = CurrentX + 5;
            } else if (x - CurrentX == 4) {
                count++;
                CurrentX = CurrentX + 4;
            } else if (x - CurrentX == 3) {
                count++;
                CurrentX = CurrentX + 3;
            } else if (x - CurrentX == 2) {
                count++;
                CurrentX = CurrentX + 2;
            } else if (x - CurrentX == 1) {
                count++;
                CurrentX = CurrentX + 1;
            }
        }

        System.out.println(count);
    }
}