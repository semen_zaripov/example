package HomeWork.ThreeDimArray;

import java.util.Random;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[][][] array = new int[3][3][3];

        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                for (int k = 0; k < array.length; k++) {
                    array[i][j][k] = random.nextInt(10);
                }
            }
        }

        array[0][1][0] = 3;
        array[0][1][1] = 12;
        array[0][1][2] = 6;

        boolean k3 = true;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                for (int k = 0; k < array.length; k++) {
                    if (j == 3 && array[i][j][k] % 3 != 0) {
                        k3 = false;
                        break;

                    }
                }
                System.out.println();

            }
        }
    }
}