package HomeWork.cycle;

import java.util.Scanner;

public class ProductNumbers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int product = 1;
        while (a > 0) {
            product = product*(a%10);
            a = a/10;
        }
        if (product % 2 == 0) {
            System.out.println(product);
            System.out.println("четное");
        } else {
            System.out.println(product);
            System.out.println("не четное");
        }
    }
}
