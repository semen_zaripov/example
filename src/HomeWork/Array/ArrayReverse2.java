package HomeWork.Array;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayReverse2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] array = new int[in.nextInt()];

        for (int i = 0; i < array.length; i++){
            array[i] = in.nextInt();
        }


        for (int i = 0; i < array.length / 2; i++){
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }

        System.out.println(Arrays.toString(array));
    }
}
