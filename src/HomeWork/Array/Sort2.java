package HomeWork.Array;

import java.util.Arrays;
import java.util.Scanner;

public class Sort2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] array = {8, 2, 7, 9, 3, 2, 5};

        insertionSort(array);

        System.out.println(Arrays.toString(array));

    }

    private static void insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int temp = array[i];
            int j = i;
            while (j > 0 && array[j - 1] > temp) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = temp;
        }
    }
}