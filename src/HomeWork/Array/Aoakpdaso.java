package HomeWork.Array;

import java.util.Arrays;

public class Aoakpdaso {
    public static void main(String[] args) {

        int[] array = {2, 4, 9, 1, 3, 5};
        for (int i = array.length; i > 0; i--) {
            for (int j = 1; j < i; j++) {
                if (array[j] < array[j - 1]) {
                    int temp = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}