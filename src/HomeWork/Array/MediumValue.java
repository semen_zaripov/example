package HomeWork.Array;

import java.util.Scanner;

public class MediumValue {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = in.nextInt();
        }
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        double a = (double)sum/array.length;
        System.out.println(a);
    }
}