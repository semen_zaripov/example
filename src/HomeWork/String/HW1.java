package HomeWork.String;

import javax.management.MBeanAttributeInfo;
import java.util.Scanner;

public class HW1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = "bbc abc ssa abc";
        String s2 = "abc";

        System.out.println(s1ToS2(s,s2));

    }

    public static boolean polindrom(String a) {
        char[] s1 = a.toCharArray();
        for (int i = 0; i < s1.length / 2; i++) {
            if (s1[i] != s1[s1.length - 1 - i]) {
                return false;
            }
        }
        return true;

    }

    public static boolean isCharUnique(String s) {
        char[] s1 = s.toCharArray();
        for (int i = 0; i < s1.length - 1; i++) {
            for (int j = i + 1; j < s1.length; j++) {
                if (s1[i] == s1[j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean s1ToS2(String s, String s2) {
        String max;
        String min;
        if (s.length() > s2.length()){
            max = s;
            min = s2;
        } else {
            max = s2;
            min = s;
        }
        char[] st1 = new char[max.length()];
        char[] st2 = new char[min.length()];

        max.getChars(0, s.length(), st1, 0);
        min.getChars(0, s2.length(), st2, 0);

        for (int i = 0;i<max.length();i++){
            for (int j = 0;j<min.length();j++){
                if(st1[i+j] != st2[j]){
                    break;
                }
                if (j == min.length() - 1){
                    return true;
                }
            }
        }
        return false;




    }
    public static String sort(String s){
        String[] s1 = s.split(" ");
        for (int i = s1.length-1;i>0;i--) {
            for (int j = 0; j < i; j++) {
                if (s1[j].charAt(0) > s1[j + 1].charAt(0)) {
                    String temp = s1[j];
                    s1[j] = s1[j + 1];
                    s1[j + 1] = temp;
                }
            }
        }
        s = "";
        for (int i = 0;i<s1.length;i++){
           s = s+" " +s1[i];

        }
        return s;
    }
}
